package user.boogo.com.mymappoc.utility;

/**
 * Created by amitdatta on 20/08/16.
 */
public class Constants {

    public static final String API_NOT_CONNECTED = "Google API not connected";
    public static final String SOMETHING_WENT_WRONG = "OOPs!!! Something went wrong...";
    public static String PlacesTag = "Google Places Auto Complete";

}
